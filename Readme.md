# Testing KAS token for validity

GitLab cluster agent token are currently unprefixed token of length 50.

## On Mac

Install [`websocat`](https://github.com/vi/websocat) via Homebrew:


```
brew install websocat
```

## On Linux

Use the `websocat-linux_x86_64` binary in this repository.

## Verfiy a token

Pick the right right [`grpc-client-cli`](https://github.com/vadimi/grpc-client-cli) for your platform from this repository.


To verify a [GitLab Agent](https://docs.gitlab.com/ee/user/clusters/agent/) token use the following command lines:

```
websocat tcp-listen:127.0.0.1:4444 wss://kas.gitlab.com/ --print-ping-rtts --ping-interval 2 --protocol ws-tunnel -v --binary -E
```

To set up a local listener which exposes the gRPC protocol behind the websocket at `kas.gitlab.com` locally.

The to verify a token use the `grpc-client-cli` with the `.proto` files from within this repo:

```
echo '{}' |grpc-client-cli --verbose --service gitlab.agent.configuration_project.rpc.ConfigurationProject -a localhost:4444 --method GetConfiguration --service gitlab.agent.agent_configuration.rpc.AgentConfiguration --proto rpc.proto -H 'Authorization: Bearer THETOKEN
```

When the token is **invalid** the repsonse should look like this:

```
[]
Error: rpc error: code = Unauthenticated desc = unauthenticated

Method: /gitlab.agent.agent_configuration.rpc.AgentConfiguration/GetConfiguration
Status: 16 Unauthenticated

Request Headers:
authorization: [Bearer THETOKEN]
user-agent: [grpc-go/1.51.0]

Response Trailers:
content-type: [application/grpc]

Request duration: 568.331685ms
Request size: 5 bytes
Response size: 50 bytes

```

When the token is **valid** the response will contain the agent configuration:

```json
{
  "configuration": {
    "gitops": null,
    "observability": null,
    "agent_id": "47582",
    "project_id": "23021668",
    "ci_access": null,
    "starboard": null,
    "project_path": "joernchen/someproject"
  },
  "commit_id": "706b2d75839b15809dfae7f15647ad87f9b598d4"
}
```
